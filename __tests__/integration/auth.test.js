const request = require('supertest');
const app = require('../../src/app');
const faker = require('faker');
const {Users} = require('../../src/models');


describe('Pruebas de integración sobre el registro de usuarios', () => {
    let newUser = {};
    let userId = 0;

    beforeAll(() => {
        newUser = {
            firstname: faker.name.findName(),
            lastname: faker.name.lastName(),
            email: faker.internet.email().toLowerCase(),
            password: 'password1'
        };
    });

    afterAll(async() => {
        await Users.destroy({where: {id: userId}});
    });

    it('Se debería de realizar un registro exitoso', async () => {
        const response = await request(app).post('/api/v1/auth/signup').send(newUser);

        //Obtener el id del usuario
        userId = response.body.id;

        //Afirmación
        expect(response.statusCode).toBe(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('email', newUser.email);
    });

    it('Se debería de mostrar un error al tratar de hacer un registro con un email repetido', async () => {
        const response = await request(app).post('/api/v1/auth/signup').send(newUser);

        //Afirmación
        expect(response.statusCode).toBe(400);
        expect(response.body).toHaveProperty('error');
    });

    it('Se debería mostrar un error al tratar de registrar un usuario con campos vacios', async () => {
        newUser.firstname = "";
        const response = await request(app).post('/api/v1/auth/signup').send(newUser);

        //Afirmación
        expect(response.statusCode).toBe(400);
        expect(response.body).toHaveProperty('error', 'Datos incorrectos');
    });

    it('Se debería mostrar un error al agregar un usuario con una contraseña de < 8 caracteres', async () => {
        newUser.firstname = "Felipe";
        newUser.password = "fel";
        const response = await request(app).post('/api/v1/auth/signup').send(newUser);

        //Afirmación
        expect(response.statusCode).toBe(400);
        expect(response.body).toHaveProperty('error', 'Datos incorrectos');
    });
});

// describe('Pruebas de integración sobre el registro de usuarios', () => {
//     it('Se debería mostrar un estado 200 al restablecer la contraseña', () => {

//     });

//     it('Se debería mostrar un estado 200 al actualizar la contraseña del usuario', () => {

//     });

//     it('Se debería mostrar un estado 400 al tratar de actualizar la contraseña del usuario con un token usado', () => {

//     });
// });