require("dotenv").config();
const faker = require("faker");
const { Users, Tokens } = require("../../src/models");
const {
    createUser,
    createToken,
    verifyResetToken,
    useResetToken,
    updatePassword,
    genResetPasswordTag,
} = require("../../src/services/auth.service");
const { sendMail } = require("../../src/config/nodemailer");
const authCtrl = require("../../src/controllers/auth.controllers");
const genToken = require("../../src/utils/create-token");

describe("Registrando un usuario", () => {
    let id = 0;
    let newUser = {};

    beforeEach(() => {
        newUser = {
            firstname: faker.name.findName(),
            lastname: faker.name.lastName(),
            email: faker.internet.email().toLowerCase(),
            password: "password1",
        };
        // jest.mock.restore();
    });

    afterEach(async () => {
        await Users.destroy({ where: { id } });
    });

    it("Debería de devolverme el id del usuario que acaba de registrar", async () => {
        let results = await createUser(newUser);

        id = results.id;

        expect(results).toHaveProperty("id", id);
    });

    it("El controlador debería de llamar a res.json con la respuesta del usuario", async () => {
        //arrange (ordenar)
        let req = {
            body: newUser,
        };

        let res = {
            status() {},
            json() {},
        };

        let resStatusSpy = jest.spyOn(res, "status");
        let resJsonSpy = jest.spyOn(res, "json");

        let next = () => {};

        //act (actuamos)
        await authCtrl.register(req, res, next);

        let response = resJsonSpy.mock.calls[0][0];
        id = response.id;

        //assert (afirmamos)
        expect(resStatusSpy).toHaveBeenCalled();
        expect(resStatusSpy.mock.calls[0][0]).toEqual(201);
    });
});

//TDD
describe("Restablecimiento de contraseña", () => {
    let user_id = 0;
    let user_id_two = 0;
    let token = "";
    let tokenDB = 0;

    beforeAll(async () => {
        newUser = {
            firstname: faker.name.findName(),
            lastname: faker.name.lastName(),
            email: faker.internet.email().toLowerCase(),
            password: "password1",
        };

        let results = await createUser(newUser);
        user_id = results.id;
    });

    beforeEach(async () => {
        newUser = {
            firstname: faker.name.findName(),
            lastname: faker.name.lastName(),
            email: faker.internet.email().toLowerCase(),
            password: "password1",
        };

        let results = await createUser(newUser);
        user_id_two = results.id;

        token = genToken(8);
        let type = "reset";

        let resultsToken = await createToken(token, user_id_two, type);
        tokenDB = resultsToken.id;
    });

    afterEach(async () => {
        await Tokens.destroy({ where: { id: tokenDB } });
        await Users.destroy({ where: { id: user_id_two } });
    });

    it("Debería de crear un token de restablecimiento", () => {
        //Longitud (token)
        let len = 8;

        //act
        let resetToken = genToken(len);

        //assert
        expect(resetToken.length).toBe(8);
    });

    it("Debería de crear un enlace de restablecimiento de contraseña", async () => {
        //crear el token
        let token = genToken(8);
        let route = process.env.RESET_PASSWORD_ROUTE;
        let content = "Da click para restablecer tu contraseña";
        //act
        let resetPasswordTag = genResetPasswordTag(
            token,
            user_id,
            route,
            content
        );
        let expectPasswordTag = `<a href='http://localhost:3000/reset-password?token=${token}&user_id=${user_id}'>${content}</a>`;
        //expect
        expect(resetPasswordTag).toBe(expectPasswordTag);
    });

    it("Debería insertar el token en la DB", async () => {
        //Crear el token
        let type = "reset";

        //Act
        let results = await createToken(token, user_id, type);

        //Assert
        expect(results).toHaveProperty("id");
        expect(results.token).toBe(token);
    });

    it.skip("Debería envíar un correo de restablecimiento de contraseña", async () => {
        const emailOptions = {
            subject: "Restablecimiento de contraseña",
            to: process.env.GOOGLE_USER,
            from: process.env.GOOGLE_USER,
            html: "<h1>Prueba de correo</h1>",
        };

        let results = await sendMail(emailOptions);

        //assert
        expect(results.response).toMatch("250");
        //SMTP 250 -> El mensaje ha sido entregado al servidor de destinatario.
    });

    it("Debería validar el token correctamente", async () => {
        //Arrange

        //Act
        let result = await verifyResetToken(token, user_id_two);

        //Assert
        expect(result).toBeTruthy();
    });

    it("Debería fallar al tratar de validar el token", async () => {
        //Arrange

        //Usamos un usuario que no existe
        let userId = 111;

        //Act
        let result = await verifyResetToken(token, userId);

        //Assert
        expect(result).toBeFalsy();
    });

    it("Debería de cambiar el estado del token", async () => {
        let result = await useResetToken(token, user_id_two);

        expect(result).toBeTruthy();
    });

    it("Debería fallar al tratar de validar el token que ya fue usado", async () => {
        //Arrange
        await useResetToken(token, user_id_two); //Usar el token

        //Act
        let result = await verifyResetToken(token, user_id_two); //Validar el token

        //Assert
        expect(result).toBeFalsy();
    });
});

describe("Actualizar contraseña con token", () => {
    let user_id = 0;
    let token = "";
    let tokenDB = 0;

    /* 
        Antes de cada prueba se creará un usuario y un token asociado a ese usuario
    */
    beforeEach(async () => {
        newUser = {
            firstname: faker.name.findName(),
            lastname: faker.name.lastName(),
            email: faker.internet.email().toLowerCase(),
            password: "password1",
        };

        let results = await createUser(newUser);
        user_id = results.id;

        token = genToken(8);
        let type = "reset";

        let resultsToken = await createToken(token, user_id, type);
        tokenDB = resultsToken.id;
    });

    //Eliminamos el token y el usuario después de cada prueba
    afterEach(async () => {
        // await Tokens.destroy({ where: { id: tokenDB } });
        // await Users.destroy({ where: { id: user_id } });
    });

    it("Debería actualizar la contraseña con un token valido", async () => {
        //Arrange
        let newPassword = "oscar12345";

        //Act
        let results = await updatePassword(token, user_id, newPassword);

        //Assert
        expect(results).toBeTruthy();
    });

    it("Debería de lanzar un error al tratar de actualizar una contraseña con un token usado", async () => {
        //Arrange
        let newPassword = "oscar12345";

        await useResetToken(token, user_id); //Vamos a cambiar el estado del token a usado

        try {
            await updatePassword(token, user_id, newPassword);
        } catch (e) {
            expect(e.name).toBe("Error");
            expect(e.message).toBe("El token es invalido");
        }
    });

    it("Debería de lanzar un error al tratar de actualizar una contraseña con un token invalido", async() => {
        //Arrange
        let newPassword = "oscar12345";
        let fakeToken = "faketoken123";

        try {
            await updatePassword(fakeToken, user_id, newPassword);
        } catch (e) {
            expect(e.name).toBe("Error");
            expect(e.message).toBe("El token es invalido");
        }
    });
});
