'use strict';

const bcrypt = require('bcryptjs');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {
      Users.hasMany(models.Tokens, {
        foreignKey: 'user_id'
      });
    }
  };
  Users.init({
    firstname: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    lastname: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        isPassword(value){
          if(value.length < 8){
            throw new Error('La contraseña debe de tener al menos 8 caracteres');
          }
        }
      }
    },
    profile_photo: DataTypes.STRING,
    verified: DataTypes.BOOLEAN,
    active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Users',
    tableName: 'users'
  });

  Users.beforeCreate(async(user) => {
    try{
      let hash = await bcrypt.hash(user.password, 8); //Generamos el hash
      user.password = hash; //Asignamos el hash a la contraseña que se agregará a la DB
      return user.password; //Finalizamos
    }catch(error){
      throw new Error("No se pudo encriptar la contraseña");
    }
  });

  Users.beforeBulkUpdate(async(user) => {
    try{
      let hash = await bcrypt.hash(user.attributes.password, 8); //Generamos el hash
      user.attributes.password = hash; //Asignamos el hash a la contraseña que se agregará a la DB
      return user.attributes.password; //Finalizamos
    }catch(error){
      throw new Error("No se pudo encriptar la contraseña");
    }
  });

  return Users;
};