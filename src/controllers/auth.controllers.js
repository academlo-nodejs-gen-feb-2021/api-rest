const {createUser, updatePassword, login} = require('../services/auth.service');


const register = async(req, res, next) => {

    const {firstname, lastname, email, password} = req.body;

    try{
        const user = await createUser({firstname, lastname, email, password});
        res.status(201);
        res.json(user);
    }catch(error){
        next(error);
    }
}

const resetPasswordCtrl = async(req, res, next) => {
    try{
        //1. Obtener el email que envía el usuario

        //2. Llama al servicio para enviar un correo electronico de restablecimiento de contraseña al usuario (sendResetPassword)

        //3. Enviar una respuesta de tipo JSON al cliente

    }catch(error){
        
    }
    //Respuesta JSON 
    //Servirá tanto para respuestas satisfactorias como erroneas esto con el fin de evitar que el usuario sepa si existe un registro de usuario en nuestro sistema.

    /* res.json({
        message: "Hemos enviado un enlace a tu correo para restablecer tu contraseña. Revisa tu bandeja de spam o solicita otro enlace de restablecimiento en caso de que no aparezca el correo.",
    }); */
}

const updatePasswordCtrl = async(req, res, next) => {
    try{
        //1. Obtener el token y el usuario a través del cuerpo de la petición
        let {token, user_id, password} = req.body;

        console.log(token, user_id, password)
        
        //2. Llamar al servicio para restablecer la contraseña (updatePassword)
        await updatePassword(token, user_id, password);

        //3. Enviar una respuesta de tipo JSON de vuelta al cliente
        res.json({
            message: "Tu contraseña ha sido restablecida.",
        });
    }catch(error){
        res.json({
            message: "Hubo un error al actualizar tu contraseña.",
            errors: error
        });
    }
}

const loginCtrl = async(req, res, next) => {
    let {email, password} = req.body;

    try{
        let token = await login(email, password);
        res.json({token});
    }catch(error){
        next(error);
    }
}

module.exports = {
    loginCtrl,
    register,
    updatePasswordCtrl,
}