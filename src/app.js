const express = require('express');
const authRoutes = require('./routes/auth.routes');
const handleError = require('./middlewares/error.middleware');

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/api/v1/', authRoutes);
app.use(handleError);

module.exports = app;