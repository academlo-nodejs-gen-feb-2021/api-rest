const {Users, Tokens} = require("../models");
const jwt = require("jsonwebtoken");
const { Op } = require("sequelize");
const bcrypt = require("bcryptjs");

const createUser = async(userObj) => {
    const {firstname, lastname, email, password} = userObj;

    try{
        let results = await Users.create({firstname, lastname, email, password});
        return results;
    }catch(error){
        throw error;
    }
}

const createToken = async(token, user_id, type) => {
    try{
        let results = await Tokens.create({token, user_id, type});
        return results;
    }catch(error){
        throw error;
    }
}

const genResetPasswordTag = (token, user_id, route, content) => {
    let baseUrl = process.env.APP_BASE_URL;
    return `<a href='${baseUrl}${route}?token=${token}&user_id=${user_id}'>${content}</a>`;
}

const verifyResetToken = async (token, userId) => {
    try{
        //Si el token existe, le pertenece al usuario y si no está usado -> token valido
        let results = await Tokens.findOne({where: {[Op.and]: [
            { token },
            { user_id: userId },
            { used: false }
        ]}});

        if(results){
            return true;
        }
        return false;
    }catch(error){
        throw error;
    }
}

const useResetToken = async (token, userId) => {
    try{
        //Si el token existe, le pertenece al usuario y si no está usado -> token valido
        let results = await Tokens.update({used: true}, { where: {[Op.and]: [
            { token },
            { user_id: userId }
        ]}});

        if(results) {
            return true
        };
        return false;

    }catch(error){
        throw error;
    }
}

const sendResetPassword = async(email) => {
    try{
        // 1. Comprobar que exista el usuario con el correo electronico (Users.findOne)

        // 2. Crear un token para el usuario y asociarlo al usuario (genToken, createToken)

        // 3. Enviar el correo electronico con el enlace de restablecimiento de contraseña (genResetPasswordTag, sendMail)

        // 4. Regresar los resultados
    }catch(error){

    }
}

const updatePassword = async(token, userId, newPassword) => {
    try{
        
        let valid = await verifyResetToken(token, userId);
        let updatePassRes = false;
        let updateResetTknRes = false;

        if(valid){
            updatePassRes = await Users.update({password: newPassword}, {where: {id: userId}});
            updateResetTknRes = await useResetToken(token, userId);
        }else{
            throw new Error("El token es invalido");
        }

        if(updatePassRes && updateResetTknRes){
            return true;
        }

        throw new Error("No se pudo actualizar la contraseña");
    }catch(error){
        throw error;
    }
}

const createJWT = (payload) => {
    let token = jwt.sign(payload, process.env.JWT_SECRET, {
        algorithm: "HS384",
        expiresIn: "1h"
    });

    return token;
}

const login = async(email, password) => {
    try{
        //Obtenemos el registro del usuario
        let user = await Users.findOne({where: {email}});
        
        //Comparamos la contraseña que envía el cliente con la que tiene guardada en la DB
        let validPass = bcrypt.compareSync(password, user.password);

        if(validPass){
            //Crear el token JWT y lo enviamos al cliente
            let userObj = {
                id: user.id,
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email
            }

            let token = createJWT(userObj);
            return token;
        }
        throw new Error("Las credenciales son incorrectas");
    }catch(error){
        throw error;
    }
}

module.exports = {
    createUser,
    createToken,
    verifyResetToken,
    useResetToken,
    updatePassword,
    genResetPasswordTag,
    createJWT,
    login
}