const nodemailer = require("nodemailer");
const googleapis = require("googleapis");

const Oauth2 = googleapis.google.auth.OAuth2;

const createTransporter = async () => {
    const oauthClient = new Oauth2(
        process.env.GOOGLE_CLIENTID,
        process.env.GOOGLE_SECRET,
        process.env.GOOGLE_REDIRECT_URI
    );

    //Fijamos el refresh token para poder obtener los tokens de acceso
    oauthClient.setCredentials({refresh_token: process.env.GOOGLE_REFRESH_TOKEN});
    
    try{
        //solicitar un token de acceso
        const accessToken = await oauthClient.getAccessToken();

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                type: 'OAuth2',
                user: process.env.GOOGLE_USER,
                accessToken,
                clientId: process.env.GOOGLE_CLIENTID,
                clientSecret: process.env.GOOGLE_SECRET,
                refreshToken: process.env.GOOGLE_REFRESH_TOKEN
            },
            tls: {
                rejectUnauthorized: false //No va a verificar la identidad del servidor
            }
        });

        return transporter;

    }catch(error){
        throw new Error(error);
    }
}

const sendMail = async(options) => {
    try{
        const gmailTransporter = await createTransporter();
        const results = await gmailTransporter.sendMail(options);
        return results;
    }catch(error){
        throw new Error(error);
    }
}


const emailOptions = {
    subject: "",
    to: "",
    from: "",
    html: ""
};

module.exports = {
    sendMail,
    emailOptions
}

