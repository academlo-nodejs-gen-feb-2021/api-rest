const {Router} = require('express');
const {register, updatePasswordCtrl, loginCtrl} = require('../controllers/auth.controllers');

const router = Router();

router.post('/auth/signup', register);
router.post('/auth/signin', loginCtrl);
router.post('/auth/logout');
router.post('/auth/forgot-password'); //1. Llamar al controlador de restablecimiento de contraseña 
router.post('/auth/reset-password', updatePasswordCtrl); //2. Llamar al controlador de actualización de contraseña
router.post('/auth/send-verification-email');
router.post('/auth/verify-email');

module.exports = router;