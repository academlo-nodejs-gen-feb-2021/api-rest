const errors = require("../utils/error-messages");

const handleError = (error, request, response, next) => {
    response
        .status(errors[error.name].status)
        .json({ error: errors[error.name].message });
};

module.exports = handleError;
