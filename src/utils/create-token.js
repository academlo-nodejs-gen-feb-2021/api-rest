const genToken = (len) => {
    let params = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let result = "";
    for(let i = 0; i<len; i++){
        let randPos = Math.floor(Math.random() * params.length);
        result += params[randPos];
    }
    return result;
}

module.exports = genToken;