module.exports = {
    SequelizeValidationError: {
        status: 400,
        message: "Datos incorrectos"
    },
    SequelizeUniqueConstraintError: {
        status: 400,
        message: "Ya existe un registro con los mismos valores"
    }
};